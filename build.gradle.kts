import org.jenkinsci.gradle.plugins.jpi.JpiExtension
import org.jenkinsci.gradle.plugins.jpi.JpiLicense
import org.jenkinsci.gradle.plugins.jpi.JpiPomCustomizer

plugins {
    java
    `maven-publish`
    id("org.jenkins-ci.jpi") version "0.43.0"
    id("io.freefair.lombok") version "6.4.1"
}

group = "cz.rewor.jenkins-ci.plugins"
description = "Configuration of credentials to access external resources"
version = "1.0." + (System.getenv("BUILD_NUMBER") ?: "0-SNAPSHOT")

jenkinsPlugin {
    jenkinsVersion.set("2.319.1")
    displayName = "URL Credentials"
    url = "https://gitlab.com/rewor/jenkinsci/url-credentials-plugin"
    gitHubUrl = "https://gitlab.com/rewor/jenkinsci/url-credentials-plugin"
    fileExtension = "jpi"
    configurePublishing = false

    licenses(closureOf<JpiExtension.Licenses> {
        license(closureOf<JpiLicense> {
            setProperty("name", "MIT")
            setProperty("distribution", "repo")
        })
    })
}

publishing {
    publications {
        create<MavenPublication>("mavenJpi") {
            artifactId = project.extensions.findByType<JpiExtension>()!!.shortName
            from(project.components["java"])
            pom {
                JpiPomCustomizer(project).customizePom(this)
                scm {
                    connection.set("scm:git:ssh://git@gitlab.com/rewor/jenkinsci/url-credentials-plugin.git")
                }
            }
        }
    }
    repositories {
        maven {
            url = uri("https://gitlab.com/api/v4/projects/4497949/packages/maven")
            name = "GitLab"
            credentials(HttpHeaderCredentials::class) {
                name = "Job-Token"
                value = System.getenv("CI_JOB_TOKEN")
            }
            authentication {
                create<HttpHeaderAuthentication>("header")
            }
        }
    }
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
    withSourcesJar()
    withJavadocJar()
}

dependencies {
    implementation("org.jenkins-ci.plugins:credentials:2.6.2")
}

tasks {
    withType<Wrapper> {
        gradleVersion = "7.4"
        distributionType = Wrapper.DistributionType.ALL
    }
    withType<JavaCompile> {
        options.encoding = "UTF-8"
    }
}
