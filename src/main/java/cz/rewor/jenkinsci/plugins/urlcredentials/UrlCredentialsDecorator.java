package cz.rewor.jenkinsci.plugins.urlcredentials;

import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.List;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.cloudbees.plugins.credentials.Credentials;
import com.cloudbees.plugins.credentials.CredentialsProvider;
import com.cloudbees.plugins.credentials.domains.URIRequirementBuilder;
import cz.rewor.jenkinsci.plugins.urlcredentials.cred.BasicHttpCredentials;
import cz.rewor.jenkinsci.plugins.urlcredentials.cred.HttpHeaderCredentials;
import hudson.Extension;
import hudson.URLConnectionDecorator;
import jenkins.model.Jenkins;
import lombok.extern.slf4j.Slf4j;

@Extension @Slf4j
public class UrlCredentialsDecorator extends URLConnectionDecorator {

    @Nonnull
    private static final String AUTHORIZATION = "Authorization";

    @Override
    public void decorate(@Nonnull URLConnection con) {
        String uri = con.getURL().toExternalForm();

        BasicHttpCredentials basic = lookup(BasicHttpCredentials.class, uri);
        if (basic != null) {
            if (con.getRequestProperty(AUTHORIZATION) != null) {
                log.info("connection to url={} already authenticated", uri);
                return;
            }

            String userInfo = String.format("%s:%s", basic.getUsername(), basic.getPassword().getPlainText());
            String auth = String.format("Basic %s", Base64.getEncoder().encodeToString(userInfo.getBytes(StandardCharsets.UTF_8)));
            con.setRequestProperty(AUTHORIZATION, auth);
            CredentialsProvider.track(Jenkins.get(), basic);
            return;
        } else {
            log.debug("no basic credentials found for uri={}", uri);
        }

        HttpHeaderCredentials httpHeaderCred = lookup(HttpHeaderCredentials.class, uri);
        if (httpHeaderCred != null) {
            String header = httpHeaderCred.getUsername();
            if (con.getRequestProperty(header) != null) {
                log.info("connection to url={} already contains header={}", uri, header);
                return;
            }

            con.setRequestProperty(header, httpHeaderCred.getPassword().getPlainText());
            CredentialsProvider.track(Jenkins.get(), httpHeaderCred);
        } else {
            log.debug("no http header credentials found for uri={}", uri);
        }
    }

    @Nullable
    private <C extends Credentials> C lookup(@Nonnull Class<C> type, @Nonnull String uri) {
        List<C> list = CredentialsProvider.lookupCredentials(type,
            Jenkins.get(),
            null,
            URIRequirementBuilder.fromUri(uri).build());

        if (list.isEmpty()) {
            log.debug("no credentials of type={} found for uri={}", type, uri);
            return null;
        }
        if (list.size() > 1) {
            log.warn("multiple credentials of type={} found for uri={}", type, uri);
        }
        return list.get(0);
    }

}
