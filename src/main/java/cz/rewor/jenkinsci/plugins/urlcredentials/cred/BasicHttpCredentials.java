package cz.rewor.jenkinsci.plugins.urlcredentials.cred;

import org.kohsuke.stapler.DataBoundConstructor;

import com.cloudbees.plugins.credentials.CredentialsScope;
import com.cloudbees.plugins.credentials.impl.UsernamePasswordCredentialsImpl;

import hudson.Extension;

import cz.rewor.jenkinsci.plugins.urlcredentials.Messages;

public class BasicHttpCredentials extends UsernamePasswordCredentialsImpl {

    private static final long serialVersionUID = 20171027;

    @DataBoundConstructor
    public BasicHttpCredentials(CredentialsScope scope, String id, String description, String username, String password) {
        super(scope, id, description, username, password);
    }

    @Extension
    public static final class DescriptorImpl extends BaseStandardCredentialsDescriptor {

        @Override
        public String getDisplayName() {
            return Messages.BasicHttpCredentials_DisplayName();
        }

        @Override
        public String getIconClassName() {
            return "icon-credentials-userpass";
        }

    }

}
