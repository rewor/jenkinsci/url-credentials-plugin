package cz.rewor.jenkinsci.plugins.urlcredentials.cred;

import com.cloudbees.plugins.credentials.CredentialsScope;
import com.cloudbees.plugins.credentials.impl.UsernamePasswordCredentialsImpl;
import cz.rewor.jenkinsci.plugins.urlcredentials.Messages;
import hudson.Extension;
import org.kohsuke.stapler.DataBoundConstructor;

public class HttpHeaderCredentials extends UsernamePasswordCredentialsImpl {

    private static final long serialVersionUID = 20171027;

    @DataBoundConstructor
    public HttpHeaderCredentials(CredentialsScope scope, String id, String description, String username, String password) {
        super(scope, id, description, username, password);
    }

    @Extension
    public static final class DescriptorImpl extends BaseStandardCredentialsDescriptor {

        @Override
        public String getDisplayName() {
            return Messages.HttpHeaderCredentials_DisplayName();
        }

        @Override
        public String getIconClassName() {
            return "icon-credentials-userpass";
        }

    }

}
